INSERT INTO unit_of_measure(name) VALUES('kg');
INSERT INTO unit_of_measure(name) VALUES('m');
INSERT INTO unit_of_measure(name) VALUES('ml');
INSERT INTO unit_of_measure(name) VALUES('m2');
INSERT INTO unit_of_measure(name) VALUES('m3');
INSERT INTO unit_of_measure(name) VALUES('piece');
INSERT INTO unit_of_measure(name) VALUES('pack');

UPDATE unit_of_measure SET created_date = NOW();
